# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    num_attendees = len(attendees_list)
    num_members = len(members_list)
    if num_attendees / num_members >= 0.5:
        return True
    else:
        return False

num_attendees = [1,2,3,4,5,6]
num_members = [1,2,3,4,5,6,7,8,9,10,11,12]

result = has_quorum(num_attendees, num_members)
print(result)
