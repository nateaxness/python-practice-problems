# Write a class that meets these requirements.
#
# Name:       BankAccount
class BankAccount:

    def __init__(self, open_balance):
        self.open_balance = open_balance

    def get_balance(self):
        return self.open_balance

    def deposit(self, add_money):
        self.open_balance += add_money
        return self.open_balance

    def withdraw(self, subtract_money):
        self.open_balance -= subtract_money
        return self.open_balance

current_balance = BankAccount(1000)
print(current_balance.deposit(500))
print(current_balance.withdraw(500))





# Required state:
#    * opening balance, the amount of money in the bank account
#
# Behavior:
#    * get_balance()      # Returns how much is in the bank account
#    * deposit(amount)    # Adds money to the current balance
#    * withdraw(amount)   # Reduces the current balance by amount
#
# Example:
#    account = BankAccount(100)
#
#    print(account.get_balance())  # prints 100
#    account.withdraw(50)
#    print(account.get_balance())  # prints 50
#    account.deposit(120)
#    print(account.get_balance())  # prints 170
#
# There is pseudocode for you to guide you.

# class BankAccount
    # method initializer(self, balance)
        # self.balance = balance

    # method get_balance(self)
        # returns the balance

    # method withdraw(self, amount)
        # reduces the balance by the amount

    # method deposit(self, amount)
        # increases the balance by the amount
